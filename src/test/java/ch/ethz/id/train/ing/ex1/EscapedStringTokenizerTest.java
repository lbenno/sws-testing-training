package ch.ethz.id.train.ing.ex1;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class EscapedStringTokenizerTest {

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testEscapedStringTokenizerString() {
        final String input = "This is a test!";
        final List<String> expected = Arrays.asList("This", "is", "a", "test!");

        EscapedStringTokenizer tokenizer = new EscapedStringTokenizer(input, " ");
        int i = 0;
        while (tokenizer.hasMoreElements()) {
            assertEquals(expected.get(i++), tokenizer.nextElement());
        }
        assertEquals(4, i);

        tokenizer = new EscapedStringTokenizer(input, " ", true);
        i = 0;
        while (tokenizer.hasMoreElements()) {
            if (i % 2 == 0) {
                assertEquals(expected.get(i / 2), tokenizer.nextElement());
            } else {
                assertEquals(" ", tokenizer.nextElement());
            }
            i += 1;
        }
        assertEquals(7, i);
    }

}
