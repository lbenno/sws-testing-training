package ch.ethz.id.train.ing.samples;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class MyClassToTestTest {

    @Mock
    private ResourceResolver resolver;
    @Mock
    private Resource resource;
    @Mock
    private Node node;
    @Mock
    private NodeIterator profiles;

    @Before
    public void setUp() throws Exception {
        when(this.resolver.getResource("path/to/person")).thenReturn(this.resource);
        when(this.resource.adaptTo(Node.class)).thenReturn(this.node);
        when(this.node.getNodes("_*")).thenReturn(this.profiles);
    }

    @Test
    public void testGetDftProfile() throws Exception {
        final Node profile1 = createProfile("profile_1", "");
        final Node profile2 = createProfile("profile_2", "default");
        final Node profile3 = createProfile("profile_3", "");
        when(this.profiles.hasNext()).thenReturn(true, true, true, false);
        when(this.profiles.nextNode()).thenReturn(profile1, profile2, profile3);

        final String profile = MyClassToTest.getDftProfile("path/to/person", this.resolver);
        assertEquals("profile_2", profile);
    }

    private Node createProfile(final String name, final String profileName) throws Exception {
        final Node out = mock(Node.class);
        final Node content = mock(Node.class);
        final Property property = mock(Property.class);
        when(out.getNode("jcr:content")).thenReturn(content);
        when(content.hasProperty("profile")).thenReturn(true);
        when(content.getProperty("profile")).thenReturn(property);
        when(property.getString()).thenReturn(profileName);
        when(out.getName()).thenReturn(name);
        return out;
    }

}
