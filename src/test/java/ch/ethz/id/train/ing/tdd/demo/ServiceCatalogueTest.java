package ch.ethz.id.train.ing.tdd.demo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

public class ServiceCatalogueTest {

    private ServiceCatalogue catalogue;

    @Before
    public void setUp() throws Exception {
        this.catalogue = new ServiceCatalogue();
    }

    @Test
    public void userCanAddServiceToCatalogue() throws Exception {
        final Procedure procedure = new Procedure("1234", "Oxygen Infusion");
        this.catalogue.add(procedure, BigDecimal.TEN);
        assertNotNull(this.catalogue.find(procedure.getId()));
        assertEquals(this.catalogue.find(procedure.getId()), procedure);
    }

    @Test
    public void catalogueReturnsNullForUnconfiguredProcId() throws Exception {
        final Procedure procedure = new Procedure("1234", "Oxygen Infusion");
        this.catalogue.add(procedure, BigDecimal.TEN);
        assertNull(this.catalogue.find("0000"));
    }

    @Test
    public void catalogueReturnsProcedureForConfiguredProcId() throws Exception {
        final Procedure procedure1 = new Procedure("1111", "Oxygen Infusion");
        final Procedure procedure2 = new Procedure("22222", "Oxygen Infusion");
        this.catalogue.add(procedure1, BigDecimal.TEN);
        this.catalogue.add(procedure2, BigDecimal.TEN);
        assertNotNull(this.catalogue.find(procedure1.getId()));
        assertNotNull(this.catalogue.find(procedure2.getId()));
    }

    @Test
    public void catalogueReturnsProcedureAndPriceForConfiguredProcId() throws Exception {
        final Procedure procedure1 = new Procedure("1111", "Oxygen Infusion");
        final Procedure procedure2 = new Procedure("22222", "Oxygen Infusion");
        this.catalogue.add(procedure1, BigDecimal.TEN);
        this.catalogue.add(procedure2, BigDecimal.ONE);

        assertEquals(this.catalogue.findPriceBy(procedure1.getId()), BigDecimal.TEN);
        assertEquals(this.catalogue.findPriceBy(procedure2.getId()), BigDecimal.ONE);
    }

}
