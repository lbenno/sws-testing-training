package ch.ethz.id.train.ing.ex2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.security.Principal;
import java.util.Iterator;

import javax.jcr.RepositoryException;
import javax.jcr.UnsupportedRepositoryOperationException;
import javax.jcr.Value;
import javax.jcr.security.AccessControlEntry;
import javax.jcr.security.AccessControlException;
import javax.jcr.security.AccessControlList;
import javax.jcr.security.AccessControlManager;
import javax.jcr.security.AccessControlPolicy;
import javax.jcr.security.AccessControlPolicyIterator;
import javax.jcr.security.Privilege;

import org.apache.jackrabbit.api.JackrabbitSession;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.Group;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class GroupManagerTest {

    @Mock
    private JackrabbitSession session;

    @Mock
    private UserManager userManager;

    @Mock
    private AccessControlManager acManager;

    @Spy
    private final AccessControlPolicy acPolicy1 = new TestACPoliciy();
    @Spy
    private final AccessControlPolicy acPolicy2 = new TestACPoliciy();

    @Mock
    private AccessControlPolicyIterator acIter;

    @Captor
    private ArgumentCaptor<Principal> principalCapt;

    private Group group;

    @Before
    public void setUp() throws Exception {
        when(this.session.getUserManager()).thenReturn(this.userManager);
        when(this.session.getAccessControlManager()).thenReturn(this.acManager);
        this.group = new TestGroup();
        when(this.userManager.createGroup(any(Principal.class), anyString())).thenReturn(this.group);

        when(this.acManager.getApplicablePolicies(anyString())).thenReturn(this.acIter);
        when(this.acIter.hasNext()).thenReturn(true, true, false);
        when(this.acIter.nextAccessControlPolicy()).thenReturn(this.acPolicy1, this.acPolicy2);
    }

    @Test
    public void testCreateGroups() throws Exception {
        final GroupManager manager = new GroupManager(this.session);
        final Group created = manager.createGroup("/path/to/group", "testGroup");
        verify(this.userManager).createGroup(this.principalCapt.capture(), eq("/path/to/group"));
        assertEquals(this.group, created);
        final Principal principal = this.principalCapt.getValue();
        assertEquals("testGroup", principal.getName());
    }

    @Test
    public void testGetAcl() throws Exception {
        final GroupManager manager = new GroupManager(this.session);

        when(this.acManager.getPolicies("path/to/1")).thenReturn(new AccessControlPolicy[] { this.acPolicy1 });
        when(this.acManager.getPolicies("path/to/2")).thenReturn(new AccessControlPolicy[] { this.acPolicy2 });

        assertEquals(this.acPolicy1, manager.getAcl("path/to/1"));
        assertEquals(this.acPolicy2, manager.getAcl("path/to/2"));

        when(this.acManager.getPolicies(anyString())).thenReturn(new AccessControlPolicy[] {});
        assertNull(manager.getAcl("another/path"));

        when(this.acManager.getPolicies(anyString())).thenReturn(new AccessControlPolicy[] { new AccessControlPolicy() {
        } });
        assertNull(manager.getAcl("another/path"));
    }

    @Test
    public void testDumpAcl() {
        fail("Not yet implemented");
    }

    @Test
    public void testAddAllowGroup() throws Exception {
        fail("Not yet implemented");
    }

    @Test
    public void testGetOrCreateRoleGroup() {
        fail("Not yet implemented");
    }

    @Test
    public void testCreateGroup() {
        fail("Not yet implemented");
    }

    // ---

    private static class TestGroup implements Group {

        public String getID() throws RepositoryException {
            // TODO Auto-generated method stub
            return null;
        }

        public boolean isGroup() {
            // TODO Auto-generated method stub
            return false;
        }

        public Principal getPrincipal() throws RepositoryException {
            // TODO Auto-generated method stub
            return null;
        }

        public Iterator<Group> declaredMemberOf() throws RepositoryException {
            // TODO Auto-generated method stub
            return null;
        }

        public Iterator<Group> memberOf() throws RepositoryException {
            // TODO Auto-generated method stub
            return null;
        }

        public void remove() throws RepositoryException {
            // TODO Auto-generated method stub

        }

        public Iterator<String> getPropertyNames() throws RepositoryException {
            // TODO Auto-generated method stub
            return null;
        }

        public Iterator<String> getPropertyNames(final String relPath) throws RepositoryException {
            // TODO Auto-generated method stub
            return null;
        }

        public boolean hasProperty(final String relPath) throws RepositoryException {
            // TODO Auto-generated method stub
            return false;
        }

        public void setProperty(final String relPath, final Value value) throws RepositoryException {
            // TODO Auto-generated method stub

        }

        public void setProperty(final String relPath, final Value[] value) throws RepositoryException {
            // TODO Auto-generated method stub

        }

        public Value[] getProperty(final String relPath) throws RepositoryException {
            // TODO Auto-generated method stub
            return null;
        }

        public boolean removeProperty(final String relPath) throws RepositoryException {
            // TODO Auto-generated method stub
            return false;
        }

        public String getPath() throws UnsupportedRepositoryOperationException, RepositoryException {
            // TODO Auto-generated method stub
            return null;
        }

        public Iterator<Authorizable> getDeclaredMembers() throws RepositoryException {
            // TODO Auto-generated method stub
            return null;
        }

        public Iterator<Authorizable> getMembers() throws RepositoryException {
            // TODO Auto-generated method stub
            return null;
        }

        public boolean isDeclaredMember(final Authorizable authorizable) throws RepositoryException {
            // TODO Auto-generated method stub
            return false;
        }

        public boolean isMember(final Authorizable authorizable) throws RepositoryException {
            // TODO Auto-generated method stub
            return false;
        }

        public boolean addMember(final Authorizable authorizable) throws RepositoryException {
            // TODO Auto-generated method stub
            return false;
        }

        public boolean removeMember(final Authorizable authorizable) throws RepositoryException {
            // TODO Auto-generated method stub
            return false;
        }

    }

    private static class TestACPoliciy implements AccessControlList {

        public AccessControlEntry[] getAccessControlEntries() throws RepositoryException {
            // TODO Auto-generated method stub
            return null;
        }

        public boolean addAccessControlEntry(final Principal principal, final Privilege[] privileges)
                throws AccessControlException, RepositoryException {
            // TODO Auto-generated method stub
            return false;
        }

        public void removeAccessControlEntry(final AccessControlEntry ace)
                throws AccessControlException, RepositoryException {
            // TODO Auto-generated method stub

        }

    }

}
