package ch.ethz.id.train.ing.samples;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.Session;
import javax.jcr.Workspace;
import javax.jcr.version.VersionManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ExtractLeadtextTest {
    private static final String PN_LEADTEXT = "leadtext";

    @Mock
    private Node node;
    @Mock
    private Node parent;
    @Mock
    private Node lead;
    @Mock
    private Property property;
    @Mock
    private Session session;
    @Mock
    private Workspace workspace;
    @Mock
    private VersionManager versionManager;
    @Mock
    private NodeIterator nodeIterator;

    @Before
    public void setUp() throws Exception {
        when(this.node.hasProperty(PN_LEADTEXT)).thenReturn(true);
        when(this.node.getProperty(PN_LEADTEXT)).thenReturn(this.property);
        when(this.property.getString()).thenReturn("some value");
        when(this.node.getParent()).thenReturn(this.parent);
        when(this.node.getSession()).thenReturn(this.session);
        when(this.session.getWorkspace()).thenReturn(this.workspace);
        when(this.workspace.getVersionManager()).thenReturn(this.versionManager);
        when(this.parent.getPath()).thenReturn("/parent/path");
        when(this.versionManager.isCheckedOut("/parent/path")).thenReturn(true);
        when(this.parent.addNode("lead_0")).thenReturn(this.lead);
        when(this.parent.getNodes("lead*")).thenReturn(this.nodeIterator);
        when(this.nodeIterator.getSize()).thenReturn(1l);
        when(this.lead.getName()).thenReturn("myLead");
        when(this.node.getName()).thenReturn("myNode");
    }

    @Test
    public void testProcess() throws Exception {
        final ExtractLeadtext step = new ExtractLeadtext();
        final Node returned = step.process(this.node);
        verify(this.node).getParent();
        verify(this.parent).addNode("lead_0");
        verify(this.lead).setProperty("leadtext", "some value");
        verify(this.lead).setProperty("sling:resourceType", "ethz/components/par/lead");
        verify(this.node).setProperty("leadtext", (String) null);
        verify(this.parent).orderBefore("myLead", "myNode");
        assertEquals(this.node, returned);
    }

}
