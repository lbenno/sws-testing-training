package ch.ethz.id.train.ing;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import ch.ethz.id.train.ing.ex0.TestMockito;
import ch.ethz.id.train.ing.ex1.EscapedStringTokenizerTest;
import ch.ethz.id.train.ing.ex2.GroupManagerTest;

@RunWith(Suite.class)
@SuiteClasses({ TestMockito.class, EscapedStringTokenizerTest.class, GroupManagerTest.class,
        ch.ethz.id.train.ing.samples.AllTests.class })
public class AllTests {

}
