package ch.ethz.id.train.ing.ex0;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;
import org.w3c.dom.Node;

/** Various tests demonstrating how to use mockito. */
public class TestMockito {

    /** Test using <code>java.sql.Connection</code>.
     * 
     * @throws Exception */
    @Test
    public void testMockito1() throws Exception {
        final Connection connection = mock(Connection.class);

        // method with different input
        when(connection.nativeSQL("0000")).thenReturn("1111");
        when(connection.nativeSQL("aaaa")).thenReturn("bbbb");

        assertEquals("1111", connection.nativeSQL("0000"));
        assertEquals("bbbb", connection.nativeSQL("aaaa"));
        assertNull(connection.nativeSQL("XYZ"));

        // method with input anyString
        when(connection.nativeSQL(anyString())).thenReturn("Hallo");
        assertEquals("Hallo", connection.nativeSQL("0000"));
        assertEquals("Hallo", connection.nativeSQL("aaaa"));
        assertEquals("Hallo", connection.nativeSQL("XYZ"));
    }

    /** Test using <code>org.w3c.dom.Node</code>.
     * 
     * @throws Exception */
    @Test
    public void testMockito2() throws Exception {
        final Node nodeChild1 = mock(Node.class);
        final Node nodeChild2 = mock(Node.class);
        final Node node = mock(Node.class);

        // method with different input
        when(node.appendChild(nodeChild1)).thenReturn(nodeChild1);
        when(node.appendChild(nodeChild2)).thenReturn(nodeChild2);

        assertEquals(nodeChild1, node.appendChild(nodeChild1));
        assertEquals(nodeChild2, node.appendChild(nodeChild2));

        final Node nodeResult = mock(Node.class);
        // method with input of any(Node.class)
        when(node.appendChild(any(Node.class))).thenReturn(nodeResult);

        assertEquals(nodeResult, node.appendChild(nodeChild1));
        assertEquals(nodeResult, node.appendChild(nodeChild2));
    }

    /** Test using <code>java.text.SimpleDateFormat</code> as spy.
     * 
     * @throws Exception */
    @Test
    public void testSpy() throws Exception {
        final SimpleDateFormat format = spy(new SimpleDateFormat("yyyy-MM-dd"));
        assertEquals("Thu Jun 27 00:00:00 CEST 2013", format.parse("2013-06-27").toString());

        final Date date = mock(Date.class);
        when(date.toString()).thenReturn("0-0-0");
        when(format.parse("1900-01-01")).thenReturn(date);

        // mock Date class used
        assertEquals("0-0-0", format.parse("1900-01-01").toString());
        assertTrue(format.parse("1900-01-01").getClass().toString()
                .startsWith("class $java.util.Date$$EnhancerByMockitoWithCGLIB"));
        // original Date class used
        assertEquals("Thu Jun 27 00:00:00 CEST 2013", format.parse("2013-06-27").toString());
        assertEquals(Date.class, format.parse("2013-06-27").getClass());
    }

}