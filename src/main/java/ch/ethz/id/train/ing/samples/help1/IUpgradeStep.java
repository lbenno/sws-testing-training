package ch.ethz.id.train.ing.samples.help1;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

/** Interface for a single update step within an update.
 * 
 * @author Luthiger */
public interface IUpgradeStep extends IBaseStep {

    /** Apply the upgrade step to the specified node.
     * 
     * @param inNode {@link Node} the JCR node to process. The node's type (i.e. the value of the property
     *            <code>sling:resourceType</code>) is guaranteed to be of the specified type (see
     *            {@link #getSlingResourceType()}).
     *            <p>
     *            Note that the node's <code>sling:resourceType</code> may have been retrieved from the node's
     *            <code>jcr:content</code>! This means that a node may be processed twice: the first time by its own
     *            node and the second time by its <code>jcr:content</code> children.
     *            </p>
     * @return {@link Node} the processed node
     * @throws RepositoryException
     * @throws UpgradeWarning */
    Node process(Node inNode) throws RepositoryException, UpgradeWarning;

    /** @return String the <code>sling:resourceType</code> this upgrade step is designed for. Must not be
     *         <code>null</code> or empty. */
    String getSlingResourceType();

}
