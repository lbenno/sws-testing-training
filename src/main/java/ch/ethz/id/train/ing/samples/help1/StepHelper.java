package ch.ethz.id.train.ing.samples.help1;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;

/** Helper class providing common functionality usable for upgrade step classes.
 * 
 * @author Luthiger */
public final class StepHelper {

    private StepHelper() {
    }

    /** Returns a unique node name for a component instance in the par sys.
     * 
     * @param inParent {@link Node} the parent node
     * @param inPrefix String the node name, e.g. <code>lead</code>
     * @return String the unique node name, e.g. <code>lead</code> or <code>lead_0</code> or <code>lead_1</code>
     * @throws RepositoryException */
    public static String getUniqueNodeName(final Node inParent, final String inPrefix) throws RepositoryException {
        final NodeIterator leadNodes = inParent.getNodes(inPrefix + "*");
        final long leadCount = leadNodes.getSize();
        return leadCount == 0 ? inPrefix : String.format("%s_%s", inPrefix, leadCount - 1);
    }

}
