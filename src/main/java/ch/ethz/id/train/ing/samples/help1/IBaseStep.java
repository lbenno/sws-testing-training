package ch.ethz.id.train.ing.samples.help1;

import org.osgi.service.cm.ConfigurationAdmin;

/** @author lbenno */
public interface IBaseStep {

    /** Setter for the injeceted OSGi services parameter object.
     * 
     * @param inConfigurationAdmin {@link ConfigurationAdmin} */
    void setServices(ConfigurationAdmin inConfigurationAdmin);

}
