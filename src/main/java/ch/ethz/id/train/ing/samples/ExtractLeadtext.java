package ch.ethz.id.train.ing.samples;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.version.VersionManager;

import org.apache.commons.lang3.StringUtils;

import ch.ethz.id.train.ing.samples.help1.AbstractUpgradeStep;
import ch.ethz.id.train.ing.samples.help1.IUpgradeStep;
import ch.ethz.id.train.ing.samples.help1.StepHelper;
import ch.ethz.id.train.ing.samples.help1.UpgradeWarning;

/** Retrieves the content of the <code>leadtext</code> property of a <code>textimage</code> node and creates a leadtext
 * node with this data that will be placed before the textimage node.
 * 
 * @author Luthiger */
public class ExtractLeadtext extends AbstractUpgradeStep implements IUpgradeStep {
    private static final String PN_LEADTEXT = "leadtext";
    private static final String NN_LEAD = "lead";
    private static final String SLING_RESOURCE_TYPE = "ethz/components/par/lead";

    public String getSlingResourceType() {
        return "ethz/components/par/textimage";
    }

    public Node process(final Node inNode) throws RepositoryException, UpgradeWarning {
        if (!inNode.hasProperty(PN_LEADTEXT)) {
            return inNode;
        }

        final String leadtext = inNode.getProperty(PN_LEADTEXT).getString();
        if (StringUtils.isNotBlank(leadtext)) {
            final Node parent = inNode.getParent();
            // checkout state: if checked in, we can't manipulate the parent node
            final VersionManager versionMgr = inNode.getSession().getWorkspace().getVersionManager();
            if (!versionMgr.isCheckedOut(parent.getPath())) {
                throw new UpgradeWarning(
                        parent.getPath()
                                + ": (step 'extract leadtext') this node is checked in an can't be manipulated! You might process the node manually.");
            }

            // create new lead node
            final Node lead = parent.addNode(StepHelper.getUniqueNodeName(parent, NN_LEAD));
            // set content
            lead.setProperty(PN_LEADTEXT, leadtext);
            lead.setProperty(SLING_RESOURCETYPE, SLING_RESOURCE_TYPE);
            // remove old lead property
            inNode.setProperty(PN_LEADTEXT, (String) null);
            // order lead node before text/image node
            parent.orderBefore(lead.getName(), inNode.getName());
        }

        return inNode;
    }

}
