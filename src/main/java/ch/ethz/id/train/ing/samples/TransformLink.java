package ch.ethz.id.train.ing.samples;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;

import org.apache.commons.lang3.StringUtils;

import ch.ethz.id.train.ing.samples.help1.AbstractUpgradeStep;
import ch.ethz.id.train.ing.samples.help1.IUpgradeStep;
import ch.ethz.id.train.ing.samples.help1.StepHelper;

/** The link component has to be replaced by a link list containing a single child link.<br />
 * Transforms
 * 
 * <pre>
 * {
 *     "sling:resourceType":"ethz/components/par/link",
 *     "jcr:primaryType":"nt:unstructured",
 *     "link":{
 *         "title":"Open Source",
 *         "hiddentext":"FOSS",
 *         "text":"Test-Link"
 *         "path":"/content/ethz_ch/de/home",
 *         "target":"newwindow",
 *         "jcr:primaryType":"nt:unstructured",
 *     }
 * }
 * </pre>
 * 
 * to
 * 
 * <pre>
 * {
 *     "title":"Komponente Linkliste:",
 *     "listFrom":"static",
 *     "sling:resourceType":"ethz/components/par/linklist",
 *     "jcr:primaryType":"nt:unstructured"
 *     "staticLinks":[
 *         "title|path|text|INTERNAL||target"
 *     ],
 * }
 * </pre>
 * 
 * where the new link item is as follows:
 * <code>title | path | text | linkType [INTERNAL, EXTERNAL, DOWNLOAD, LOGIN]|| target [samewindow, newwindow]</code>.
 * 
 * @author Luthiger */
public class TransformLink extends AbstractUpgradeStep implements IUpgradeStep {
    private static final String SLING_RESOURCETYPE = "sling:resourceType";
    private static final String PN_STATIC = "staticLinks";
    private static final String NN_LINK = "link";
    private static final String SLING_RESOURCE_TYPE_L = "ethz/components/par/link";
    private static final String NN_LINKLIST = "linklist";
    private static final String SLING_RESOURCE_TYPE_LL = "ethz/components/par/linklist";

    // properties of link list component
    private static final String PN_TITLE = "title";
    private static final String PN_LIST = "listFrom";
    private static final String PV_TITLE = ""; // Linklist
    private static final String PV_LIST = "static";

    public Node process(final Node inNode) throws RepositoryException {
        final Node parent = inNode.getParent();
        // check parent's resource type, must not be 'linklist'. Such nodes are upgrades with step 'UpgradeLinkList'
        if (checkResourceType(parent, SLING_RESOURCE_TYPE_LL)) {
            return inNode;
        }

        // if we don't have a link child node, we have nothing to do (maybe node is upgraded yet)
        if (!inNode.hasNode(NN_LINK)) {
            return inNode;
        }

        // create new linklist node
        final Node linklist = parent.addNode(StepHelper.getUniqueNodeName(parent, NN_LINKLIST));
        // set content
        linklist.setProperty(SLING_RESOURCETYPE, SLING_RESOURCE_TYPE_LL);
        linklist.setProperty(PN_TITLE, PV_TITLE);
        linklist.setProperty(PN_LIST, PV_LIST);
        linklist.setProperty(PN_STATIC, new String[] { createLink(inNode.getNode(NN_LINK)) });
        // remove obsolete link node
        parent.orderBefore(linklist.getName(), inNode.getName());
        inNode.remove();

        return linklist;
    }

    private String createLink(final Node inNode) throws PathNotFoundException, RepositoryException {
        String title = getProperty(inNode, "title");
        String text = getProperty(inNode, "text");
        title = StringUtils.isBlank(title) ? (StringUtils.isBlank(text) ? "-" : text) : title;
        text = StringUtils.isBlank(text) ? title : text;
        return String.format("%s|%s|%s|%s||%s", title, getProperty(inNode, "path"),
                text, getLinkType(inNode), getProperty(inNode, "target"));
    }

    private String getLinkType(final Node inNode) throws PathNotFoundException, RepositoryException {
        if ("true".equalsIgnoreCase(getProperty(inNode, "login"))) {
            return "LOGIN";
        }
        return "EXTERNAL";
    }

    public String getSlingResourceType() {
        return SLING_RESOURCE_TYPE_L;
    }

}
