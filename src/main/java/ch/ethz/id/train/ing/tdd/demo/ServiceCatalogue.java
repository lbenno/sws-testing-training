package ch.ethz.id.train.ing.tdd.demo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ServiceCatalogue {

    private final List<Procedure> procedures = new ArrayList<Procedure>();
    private final Map<String, BigDecimal> prices = new HashMap<String, BigDecimal>();

    public void add(final Procedure procedure, final BigDecimal price) {
        this.procedures.add(procedure);
        this.prices.put(procedure.getId(), price);
    }

    public Procedure find(final String id) {
        for (final Procedure procedure : this.procedures) {
            if (id.equals(procedure.getId())) {
                return procedure;
            }
        }
        return null;
    }

    public BigDecimal findPriceBy(final String id) {
        return this.prices.get(id);
    }

}
