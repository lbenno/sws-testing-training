package ch.ethz.id.train.ing.samples.help1;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.osgi.service.cm.ConfigurationAdmin;

/** Base class for upgrade steps.
 * 
 * @author Luthiger */
public abstract class AbstractUpgradeStep implements IUpgradeStep {
    protected static final String JCR_CONTENT_NODE = "jcr:content";
    protected static final String SLING_RESOURCETYPE = "sling:resourceType";

    private ConfigurationAdmin services;

    protected AbstractUpgradeStep() {
        // Protected constructor
    }

    public void setServices(final ConfigurationAdmin inConfigurationAdmin) {
        this.services = inConfigurationAdmin;
    }

    protected ConfigurationAdmin getServices() {
        return this.services;
    }

    /** @return String retrieves the value of the specified property or an empty string, if the property is not
     *         available
     * @throws RepositoryException */
    protected String getProperty(final Node inNode, final String inProperty) throws RepositoryException {
        return inNode.hasProperty(inProperty) ? inNode.getProperty(inProperty).getString() : "";
    }

    /** Checks the passed node's specified value.
     * 
     * @param inNode {@link Node}
     * @param inPropName String the name of the property to check
     * @param inPropValue String the value of the property to check
     * @return boolean <code>true</code> if the passed node has the expected value in the specified property
     * @throws RepositoryException */
    protected boolean hasProperty(final Node inNode, final String inPropName, final String inPropValue)
            throws RepositoryException {
        return inPropValue.equals(getProperty(inNode, inPropName));
    }

    /** Checks the passed node's <code>sling:resourceType</code> value.
     * 
     * @param inNode {@link Node}
     * @param inValue String the expected resource type
     * @return boolean <code>true</code> if the passed node has the expected type
     * @throws RepositoryException */
    protected boolean checkResourceType(final Node inNode, final String inValue) throws RepositoryException {
        return hasProperty(inNode, SLING_RESOURCETYPE, inValue);
    }

    /** Default implementation of reversing the upgrade. <br />
     * This implementation leaves the upgraded node unchanged. The node is NOT put to it's original state.
     * 
     * @param inNode {@link Node}
     * @return {@link Node}
     * @throws RepositoryException
     * @throws UpgradeWarning */
    public Node reverse(final Node inNode) throws RepositoryException {
        return inNode;
    }

    /** @param inNode {@link Node}
     * @return boolean <code>true</code> if the specified node's name is <code>jcr:content</code> */
    protected boolean isJcrContent(final Node inNode) {
        try {
            return JCR_CONTENT_NODE.equals(inNode.getName());
        } catch (final RepositoryException exc) { // NOPMD
            // intentionally left empty
        }
        return false;
    }

}
