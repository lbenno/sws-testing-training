package ch.ethz.id.train.ing.ex2;

import java.security.Principal;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.security.AccessControlEntry;
import javax.jcr.security.AccessControlList;
import javax.jcr.security.AccessControlManager;
import javax.jcr.security.AccessControlPolicy;
import javax.jcr.security.AccessControlPolicyIterator;
import javax.jcr.security.Privilege;

import org.apache.jackrabbit.api.JackrabbitSession;
import org.apache.jackrabbit.api.security.user.Group;
import org.apache.jackrabbit.api.security.user.UserManager;

/** Automatically creates the groups assigned to a new home page following the concept of ETH web.
 * 
 * @author schachri */
public class GroupManager {

    private static final String ASSOCIATES = "associates";
    private static final String SPECIALINTEREST = "specialinterest";
    private static final String GROUPNAME_PREFIX = "eth-";
    private static final String GROUP_ROOT = "/home/groups";
    private static final String COMMON_GROUP_PATH = "ethz/web";

    /** The roles from the role concept. */
    public enum Role {
        READER(Privilege.JCR_READ), AUTHOR(Privilege.JCR_WRITE, Privilege.JCR_NODE_TYPE_MANAGEMENT,
                Privilege.JCR_LOCK_MANAGEMENT,
                Privilege.JCR_VERSION_MANAGEMENT), EDITOR(Privilege.JCR_ALL), CHIEFEDITOR(Privilege.JCR_ALL);

        String[] privileges;

        Role(final String... privileges) {
            this.privileges = privileges;
        }

        public String[] getPrivileges() {
            return this.privileges;
        }
    }

    private final JackrabbitSession session;
    private final UserManager um;
    private final AccessControlManager am;

    /** Creates an instance for a session (group management rights needed).
     * 
     * @param session the session
     * @throws RepositoryException if fails to user and access control manager */
    public GroupManager(final JackrabbitSession session) throws RepositoryException {
        this.session = session;
        this.um = session.getUserManager();
        this.am = session.getAccessControlManager();
    }

    /** Creates the groups for a given page.
     * 
     * @param inNode the node of the page
     * @throws RepositoryException if fails */
    public void createGroups(final Node inNode) throws RepositoryException {
        final String pagePath = inNode.getPath();
        final AccessControlList acl = getAcl(pagePath);

        final String damPath = pagePath.replaceFirst("/content/", "/content/dam/ethz/");
        AccessControlList damAcl = null;
        if (this.session.nodeExists(damPath)) {
            damAcl = getAcl(damPath);
        }

        for (final Role role : Role.values()) {
            final Group group = getOrCreateRoleGroup(pagePath, role);
            addAllowGroup(acl, group, role);
            if (damAcl != null) {
                addAllowGroup(damAcl, group, role);
            }

            if (role == Role.CHIEFEDITOR) {
                final String groupParentPath = getParentPath(group.getPath());
                final AccessControlList groupAcl = getAcl(groupParentPath);
                addAllowGroupForMembershipModification(groupAcl, group);
                this.am.setPolicy(groupParentPath, groupAcl); // need to set again after modification
            }
        }

        this.am.setPolicy(pagePath, acl); // need to set again after modification
        if (damAcl != null) {
            this.am.setPolicy(damPath, damAcl); // need to set again after modification
        }
    }

    private String getParentPath(final String path) {
        return path.replaceAll("(.*)/[^/]*$", "$1");
    }

    /** Returns the access control list for a given path in JCR repository. Creates it if not yet installed.
     * 
     * @param path the path
     * @return the the ACL
     * @throws RepositoryException if fails */
    public AccessControlList getAcl(final String path)
            throws RepositoryException {

        ensureAclInstalled(path);

        AccessControlList acl = null;
        final AccessControlPolicy[] policies = this.am.getPolicies(path);
        for (final AccessControlPolicy accessControlPolicy : policies) {
            if (accessControlPolicy instanceof AccessControlList) {
                acl = (AccessControlList) accessControlPolicy;
                break;
            }
        }

        return acl;
    }

    /** Dumps a given ACL to console.
     * 
     * @param acl the ACL
     * @throws RepositoryException if fails */
    public void dumpAcl(final AccessControlList acl) throws RepositoryException {
        final AccessControlEntry[] aces = acl.getAccessControlEntries();
        for (final AccessControlEntry accessControlEntry : aces) {
            System.out.println(accessControlEntry.getPrincipal().getName());
            final Privilege[] priv = accessControlEntry.getPrivileges();
            for (final Privilege privilege : priv) {
                System.out.println(privilege.getName());
            }
        }
    }

    /** Ensures that the ACL policy has been created on a given path.
     * 
     * @param path
     * @throws RepositoryException if fails */
    private void ensureAclInstalled(final String path) throws RepositoryException {
        final AccessControlPolicyIterator iter = this.am.getApplicablePolicies(path);
        while (iter.hasNext()) {
            final AccessControlPolicy accessControlPolicy = iter.nextAccessControlPolicy();
            if (accessControlPolicy instanceof AccessControlList) {
                final AccessControlList acl = (AccessControlList) accessControlPolicy;
                this.am.setPolicy(path, acl);
            }
        }
    }

    /** Adds an allow rule to an ACL, for the given group, according to the given role.
     * 
     * @param acl the ACL
     * @param group the group to allow
     * @param role the role
     * @throws RepositoryException */
    public void addAllowGroup(final AccessControlList acl, final Group group,
            final Role role)
                    throws RepositoryException {

        final String[] privilegeNames = role.getPrivileges();
        final Privilege[] privileges = new Privilege[privilegeNames.length];
        for (int i = 0; i < privileges.length; i++) {
            privileges[i] = this.am.privilegeFromName(privilegeNames[i]);
        }

        acl.addAccessControlEntry(group.getPrincipal(), privileges);
    }

    /** Adds an allow rule to an ACL, for the given group, to enable group membership modification.
     * 
     * @param acl the ACL of a subtree in /home/groups
     * @param group the group to allow
     * @throws RepositoryException */
    private void addAllowGroupForMembershipModification(final AccessControlList acl, final Group group)
            throws RepositoryException {

        final Privilege[] privileges = new Privilege[] { this.am.privilegeFromName(Privilege.JCR_MODIFY_PROPERTIES) };

        acl.addAccessControlEntry(group.getPrincipal(), privileges);
    }

    /** Returns the group for a given role on a given page. Creates it if missing.
     * 
     * @param pagePath the path to the page in JCR
     * @param role the role
     * @return the group
     * @throws RepositoryException */
    public Group getOrCreateRoleGroup(final String pagePath, final Role role)
            throws RepositoryException {

        final String groupName = generateGroupName(pagePath, role);
        final String groupPath = COMMON_GROUP_PATH + pagePath;

        Group group = (Group) this.um.getAuthorizableByPath(GROUP_ROOT + "/" + groupPath + "/" + groupName);
        if (group == null) {
            group = createGroup(groupPath, groupName);

            final Group contributorGroup = getOrCreateContributorGroup();
            contributorGroup.addMember(group);
        }

        return group;
    }

    /** Returns the basic contributor group that all role groups are member of.
     * 
     * @return the group
     * @throws RepositoryException if fails */
    private Group getOrCreateContributorGroup() throws RepositoryException {

        final String groupName = GROUPNAME_PREFIX + "contributor";

        Group group = (Group) this.um.getAuthorizableByPath(GROUP_ROOT + "/" + COMMON_GROUP_PATH + "/" + groupName);
        if (group == null) {
            group = createGroup(COMMON_GROUP_PATH, groupName);
        }

        return group;
    }

    /** Generic group creation command.
     * 
     * @param groupPath path in repository
     * @param groupName name
     * @return group
     * @throws RepositoryException if fails */
    public Group createGroup(final String groupPath, final String groupName)
            throws RepositoryException {

        final Principal p = new Principal() {
            public String getName() {
                return groupName;
            }
        };

        return this.um.createGroup(p, groupPath);
    }

    /** Builds the role group's name of page by page path and role.
     * 
     * @param pagePath the path
     * @param role the role
     * @return the name */
    private String generateGroupName(final String pagePath, final Role role) {
        final StringBuffer groupName = new StringBuffer(pagePath.length());

        groupName.append(GROUPNAME_PREFIX);

        final String[] segments = pagePath.split("/");

        int start = 2;
        if (segments.length > 2 && ASSOCIATES.equals(segments[2]) || SPECIALINTEREST.equals(segments[2])) {
            // skip prefix of associates and specialinterest
            start = 3;
        }

        for (int i = start; i < segments.length; i++) {
            groupName.append(segments[i]);
            groupName.append("-");
        }

        groupName.append(role.name().toLowerCase());

        return groupName.toString();
    }

}
