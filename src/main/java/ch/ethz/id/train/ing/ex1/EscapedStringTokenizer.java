package ch.ethz.id.train.ing.ex1;

import java.util.Enumeration;
import java.util.NoSuchElementException;

/** This class implements a string tokenizer like the </code>java.util.StringTokenizer</code>. Additionally it's able to
 * parse a string with escaped tokens. For example, you can define the delimiter and a release character which releases
 * the delimiter-character from its rule as delimiter. */
public class EscapedStringTokenizer implements Enumeration<String> { // NOPMD by lbenno
    private transient int currentPosition;
    private transient final int maxPosition;
    private transient final char[] string;
    private transient final String delimiters;
    private transient final boolean retTokens;
    private transient char escapeCharacter;
    private transient boolean checkEscapedDelimiter;
    private transient boolean keepEscaped;

    /** Creates a new instance of EscapedStringTokenizer, with default delimiters (space, tab, newline).
     *
     * @param inString The string to be tokenized */
    public EscapedStringTokenizer(final String inString) {
        this(inString, " \t\n\r", false);
    }

    /** Creates a new instance of EscapedStringTokenizer.
     *
     * @param inString The string to be tokenized
     * @param inDelimiter A string containing all delimiters */
    public EscapedStringTokenizer(final String inString, final String inDelimiter) {
        this(inString, inDelimiter, false);
    }

    /** Creates a new instance of EscapedStringTokenizer.
     *
     * @param inString The string to be tokenized
     * @param inDelimiter A string containing all delimiters
     * @param inReturnTokens If true, return delimiters as tokens. Different in <code>StringTokenizer</code> */
    public EscapedStringTokenizer(final String inString, final String inDelimiter, final boolean inReturnTokens) {
        this.currentPosition = 0;
        this.string = new char[inString.length()];
        inString.getChars(0, inString.length(), this.string, 0);
        this.maxPosition = inString.length() - 1;
        this.delimiters = inDelimiter;
        this.retTokens = inReturnTokens;
    }

    /** Creates a new instance of EscapedStringTokenizer.
     *
     * @param inString The string to be tokenized
     * @param inDelimiter A string containing all delimiters
     * @param inReturnTokens If true, return delimiters as tokens. Different in <code>StringTokenizer</code>
     * @param inEscapeCharacter char */
    public EscapedStringTokenizer(final String inString, final String inDelimiter, final boolean inReturnTokens,
            final char inEscapeCharacter) {
        this(inString, inDelimiter, inReturnTokens);
        this.escapeCharacter = inEscapeCharacter;
        this.checkEscapedDelimiter = true;
    }

    /** Creates a new instance of EscapedStringTokenizer.
     *
     * @param inString The string to be tokenized
     * @param inDelimiter A string containing all delimiters
     * @param inReturnTokens If true, return delimiters as tokens. Different in <code>StringTokenizer</code>
     * @param inEscapeCharacter char
     * @param inKeepEscapedCharacter If true, escaped characters will be kept escaped. */
    public EscapedStringTokenizer(final String inString, final String inDelimiter, final boolean inReturnTokens,
            final char inEscapeCharacter, final boolean inKeepEscapedCharacter) {
        this(inString, inDelimiter, inReturnTokens);
        this.escapeCharacter = inEscapeCharacter;
        this.checkEscapedDelimiter = true;
        this.keepEscaped = inKeepEscapedCharacter;
    }

    /** @return boolean <code>true</code> if there are more elements/tokens are in the string */
    public boolean hasMoreElements() {

        if (this.currentPosition > this.maxPosition) {
            return false;
        }
        if (!isDelimiter(this.currentPosition) || this.retTokens) {
            return true;
        }

        int lTempPos = this.currentPosition;
        while (lTempPos <= this.maxPosition && isDelimiter(lTempPos)) {
            lTempPos++;
        }
        return lTempPos <= this.maxPosition;
    }

    /** @param inPos int
     * @return boolean <code>true</code> if the character at specified position (inPos) is a delimiter. */
    private boolean isDelimiter(final int inPos) {

        if (this.delimiters.indexOf(this.string[inPos]) == -1) {
            return false;
        }

        if (inPos == 0 || !this.checkEscapedDelimiter) {
            return true;
        }

        int lTempPos = this.currentPosition - 1;
        while (lTempPos >= 0 && this.string[lTempPos] == this.escapeCharacter) {
            lTempPos--;
        }

        return (this.currentPosition - lTempPos + 1) % 2 == 0;
    }

    /** @return String the next element */
    public String nextElement() { // NOPMD by lbenno
        if (!hasMoreElements()) {
            throw new NoSuchElementException();
        }

        final StringBuilder outElement = new StringBuilder(100);

        // if delimiters are returned as tokens and character at current position is a delimiter
        if (isDelimiter(this.currentPosition) && this.retTokens) {
            this.currentPosition++;
            return outElement.append(this.string[this.currentPosition - 1]).toString();
        }

        // skip the delimiters
        while (this.currentPosition <= this.maxPosition && isDelimiter(this.currentPosition)) {
            this.currentPosition++;
        }

        boolean lLastWasEscapeChar = false;
        while (this.currentPosition <= this.maxPosition && !isDelimiter(this.currentPosition)) {
            final char lChar = this.string[this.currentPosition];
            if (this.keepEscaped) {
                outElement.append(lChar);
            } else {
                if (lChar == this.escapeCharacter) {
                    if (lLastWasEscapeChar) {
                        lLastWasEscapeChar = false;
                        outElement.append(lChar);
                    } else {
                        lLastWasEscapeChar = true;
                    }
                } else {
                    lLastWasEscapeChar = false;
                    outElement.append(lChar);
                }
            }
            this.currentPosition++;
        }

        return outElement.toString();
    }

}
