package ch.ethz.id.train.ing.samples.help1;

/** Warning exception in case of non fatal upgrade problems. This warning can be used in an upgrade step's process
 * method.
 * 
 * @author Luthiger
 * @see ch.ethz.id.train.ing.samples.help1.wai.wcms.upgrade.IUpgradeStep#process(javax.jcr.Node) */
@SuppressWarnings("serial")
public class UpgradeWarning extends Exception {

    private final String message;

    /** @param inMessage */
    public UpgradeWarning(final String inMessage) {
        super(inMessage);
        this.message = inMessage;
    }

    /** @return String retrieves the warning */
    public String getWarning() {
        return this.message;
    }

}
