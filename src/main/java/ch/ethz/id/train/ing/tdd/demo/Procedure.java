package ch.ethz.id.train.ing.tdd.demo;

public class Procedure {

    private final String id;
    private final String service;

    public Procedure(final String id, final String service) {
        this.id = id;
        this.service = service;
    }

    public String getId() {
        return this.id;
    }

    public String getService() {
        return this.service;
    }

}
