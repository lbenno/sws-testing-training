package ch.ethz.id.train.ing.samples;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;

import org.apache.sling.api.resource.ResourceResolver;

/** Sample class to test mockito with multi-valued parameter. */
public class MyClassToTest {
    private static final String JCR_CONTENT_NODE = "jcr:content";
    private static final String PROFILE = "profile";
    private static final String PV_DFT_PROFILE = "default";

    /** Returns the name of a person's default profile. The default profile is a node marked with the property
     * <code>profile:default</code>.
     * 
     * @param inPath String path to the node containing the profiles
     * @param inResolver {@link ResourceResolver}
     * @return String the name of the default profile
     * @throws RepositoryException */
    public static String getDftProfile(final String inPath, final ResourceResolver inResolver)
            throws RepositoryException {
        final Node node = inResolver.getResource(inPath).adaptTo(Node.class);
        final NodeIterator profiles = node.getNodes("_*");

        // check for the marked profile
        while (profiles.hasNext()) {
            final Node profile = profiles.nextNode();
            final Node content = profile.getNode(JCR_CONTENT_NODE);
            if (content.hasProperty(PROFILE) && PV_DFT_PROFILE.equals(content.getProperty(PROFILE).getString())) {
                return profile.getName();
            }
        }

        // return the first profile
        return node.getNodes("_*").nextNode().getName();
    }
}
